set :application, "Opengestia"
set :domain,      "opengestia.juniorsmd.org"
set :deploy_to,   "/ebs/www/opengestia/"
set :app_path,    "app"

set :user,        "ubuntu"
set :use_sudo, 	  false

set :ssh_options, {:forward_agent => true}
default_run_options[:pty] = true

set :repository,  "git@bitbucket.org:vicentgodella/opengestia.git"
set :scm,         :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, `subversion` or `none`

set :branch,      "master"

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain                         # This may be the same as your `Web` server
role :db,         domain, :primary => true       # This is where Rails migrations will run

set :keep_releases, 5

set :shared_files,      ["app/config/parameters.yml"]

set :shared_children,     [app_path + "/cache", app_path + "/logs"]

set :update_vendors, false
#set :composer_bin, '/usr/local/bin/composer'
set :use_composer, true

set :admin_runner, "ubuntu"

set :dump_assetic_assets, true
set :interactive_mode, true

logger.level = Logger::MAX_LEVEL


# Change ACL on the app/logs and app/cache directories
after 'deploy', 'deploy:update_acl'

# This is a custom task to set the ACL on the app/logs and app/cache directories
namespace :deploy do

  task :update_acl, :roles => :app do
    shared_dirs = [
      app_path + "/logs",
      app_path + "/cache"
    ]
    
    # add group write permissions
    run "cd #{latest_release} && sudo chmod 777 #{shared_dirs.join(' ')} -R"
    # Allow directories to be writable by webserver and this user
    #run "cd #{latest_release} && sudo setfacl -R -m u:www-data:rwx -m u:#{user}:rwx #{shared_dirs.join(' ')}"
    #run "cd #{latest_release} && sudo setfacl -dR -m u:www-data:rwx -m u:#{user}:rwx #{shared_dirs.join(' ')}"
  end
end

task :upload_parameters do
  origin_file = "app/config/parameters.yml"
  destination_file = shared_path + "/app/config/parameters.yml" # Notice the
  shared_path

  try_sudo "mkdir -p #{File.dirname(destination_file)}"
  top.upload(origin_file, destination_file)
end

after "deploy:setup", "upload_parameters"
after "deploy", "deploy:cleanup"