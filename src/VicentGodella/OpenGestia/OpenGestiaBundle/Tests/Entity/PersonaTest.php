<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona;

class PersonaTest extends WebTestCase
{
    protected $persona;
    protected static $validator;
    protected static $kernel;

    public static function setUpBeforeClass()
    {
        $kernel = self::getKernelClass();
        $kernel = new $kernel('dev', true);
        $kernel->boot();

        self::$kernel = $kernel;
        self::$validator = $kernel->getContainer()->get('validator');
    }

    protected function setUp()
    {
        $this->persona = new Persona();
        $this->persona->setNombre('Juan');
        $this->persona->setApellidos('Martínez González');
        $this->persona->setDireccion('C/ Perdida, 10');
        $this->persona->setFechaNacimiento(new \DateTime('02/10/2000'));
        $this->persona->setCodigoPostal('46000');
        $this->persona->setPoblacion('Valencia');
        $this->persona->setCentro('Micentro');
    }

    public function testNombreNoPuedeSerVacio()
    {
        $this->persona->setNombre('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testApellidosNoPuedenSerVacio()
    {
        $this->persona->setApellidos('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testDireccionNoPuedeSerVacio()
    {
        $this->persona->setDireccion('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testFechaNacimientoNoPuedeSerVacio()
    {
        $this->persona->setFechaNacimiento('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testCodigoPostalNoPuedeSerVacio()
    {
        $this->persona->setCodigoPostal('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testPoblacionNoPuedeSerVacio()
    {
        $this->persona->setPoblacion('');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testFechaNacimientoDebeTenerFormatoAdecuado()
    {
        $this->persona->setFechaNacimiento('07/05/2000');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testTelefonoDebeTenerFormatoAdecuado()
    {
        $this->persona->setTelefono('96123456');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(0, $violationList->count());
    }

    public function testTelefonoNoPedeTenerLetras()
    {
        $this->persona->setTelefono('9612345a');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testTelefonoNoDebeTenerMenosDe8digitos()
    {
        $this->persona->setTelefono('9612345');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testTelefonoNoDebeTenerMasDe9digitos()
    {
        $this->persona->setTelefono('6001234567');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testEmailDebeTenerFormatoAdecuado()
    {
        $this->persona->setEmail('jose@opengestia.org');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(0, $violationList->count());
    }

    public function testEmailDebeTenerDominioDetrasArroba()
    {
        $this->persona->setEmail('jose@');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

    public function testDNI()
    {
        $this->persona->setDNI('52654994J');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(0, $violationList->count());
    }

    public function testDNIFalso()
    {
        $this->persona->setDNI('52654994A');

        $violationList = self::$validator->validate($this->persona);
        $this->assertEquals(1, $violationList->count());
    }

	public function testFalloNoTieneFormatoAdecuado()
	{
		$this->persona->setDNI('ASFAASFSDFA');

		$violationList = self::$validator->validate($this->persona);
		$this->assertEquals(1, $violationList->count());
	}

	public function testAdmiteGuiones()
	{
		$this->persona->setDNI('30000000-L');

		$violationList = self::$validator->validate($this->persona);
		$this->assertEquals(0, $violationList->count());
	}
}
