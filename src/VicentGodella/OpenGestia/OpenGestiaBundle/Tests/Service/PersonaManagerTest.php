<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Tests\Service;

use VicentGodella\OpenGestia\OpenGestiaBundle\Service\Manager\PersonaManager;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class PersonaManagerrTest extends \PHPUnit_Framework_TestCase
{
    protected $personaManager;
    protected $emMock;

	public function setUp()
    {
        $this->emMock = $this->getMockBuilder("Doctrine\ORM\EntityManager")
            ->disableOriginalConstructor()
            ->getMock();

        $this->personaManager = new PersonaManager($this->emMock);
    }

    public function testDarDeBaja()
    {
        $this->emMock
            ->expects($this->once())
            ->method('persist');
        $this->emMock
            ->expects($this->once())
            ->method('flush');

        $personaMock = $this->getMock("VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona");
        $personaMock
            ->expects($this->once())
            ->method('setEstado')
            ->with(1);

        $this->personaManager->darDeBaja($personaMock);
    }

    public function testDarDeBajaDefinitiva()
    {
        $this->emMock
            ->expects($this->once())
            ->method('persist');
        $this->emMock
            ->expects($this->once())
            ->method('flush');

        $personaMock = $this->getMock("VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona");
        $personaMock
            ->expects($this->once())
            ->method('setEstado')
            ->with(2);

        $this->personaManager->darDeBajaDefinitiva($personaMock);
    }

    public function testExistePersonaEnElCentro()
    {
        $personaMock = $this->getMock("VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona");
        $personaMock
            ->expects($this->once())
            ->method('getNombre')
            ->will($this->returnValue('Ricardo'));
        $personaMock
            ->expects($this->once())
            ->method('getApellidos')
            ->will($this->returnValue('Rodríguez Pérez'));
        $personaMock
            ->expects($this->once())
            ->method('getFechaNacimiento')
            ->will($this->returnValue('1996/04/01'));

        $repositoryMock = $this->getMockBuilder("Doctrine\ORM\EntityRepository")
            ->disableOriginalConstructor()
            ->getMock();
        $repositoryMock
            ->expects($this->once())
            ->method('findBy')
            ->with(array(
                'nombre' => 'Ricardo',
                'apellidos' => 'Rodríguez Pérez',
                'fecha_nacimiento' => '1996/04/01'
            ))
            ->will($this->returnValue(array($personaMock)));

        $this->emMock
            ->expects($this->once())
            ->method('getRepository')
            ->with('OpenGestiaBundle:Persona')
            ->will($this->returnValue($repositoryMock));

        $this->assertTrue($this->personaManager->existePersonaEnCentro($personaMock));
    }
}
