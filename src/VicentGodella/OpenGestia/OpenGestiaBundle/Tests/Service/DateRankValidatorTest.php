<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Tests\Service;

use VicentGodella\OpenGestia\OpenGestiaBundle\Service\Dater;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class DateRankValidatorTest extends \PHPUnit_Framework_TestCase
{
	protected static $validator;
	protected static $kernel;

	public static function setUpBeforeClass()
	{
		self::$kernel = new \AppKernel('dev', true);
		self::$kernel->boot();

		self::$validator = self::$kernel->getContainer()->get('validator');
	}

	public function get($serviceId)
	{
		return self::$kernel->getContainer()->get($serviceId);
	}

	public function testValidRankIfIniDateGreaterThanEndDate()
	{
		$dateRankChecker = $this->get('open_gestia.date_rank_checker');

		$stubDater = $this->getMock('VicentGodella\OpenGestia\OpenGestiaBundle\Service\Dater');
		$stubDater->expects($this->any())
			->method('getCurrentTime')
			->will($this->returnValue('19/09'));

		$dateRankChecker->setDater($stubDater);

		$dateRankChecker->setRank(array('iniDate' => '01/09','endDate' => '31/03'));

		$this->assertEquals(true, $dateRankChecker->isRankValid());
	}


	public function testInvalidRankIfIniDateGreaterThanEndDate()
	{
		$dateRankChecker = $this->get('open_gestia.date_rank_checker');

		$stubDater = $this->getMock('VicentGodella\OpenGestia\OpenGestiaBundle\Service\Dater');
		$stubDater->expects($this->any())
			->method('getCurrentTime')
			->will($this->returnValue('18/07'));

		$dateRankChecker->setDater($stubDater);

		$dateRankChecker->setRank(array('iniDate' => '01/09','endDate' => '31/03'));

		$this->assertEquals(false, $dateRankChecker->isRankValid());
	}
}
