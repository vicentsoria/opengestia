<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class DateRankConstraintValidator extends ConstraintValidator
{
	protected $dateRankChecker;

	public function __construct($dateRankChecker)
	{
		$this->dateRankChecker = $dateRankChecker;
	}

	public function validate($persona, Constraint $constraint)
	{
		if($persona->getEstado() == Persona::ESTADO_BAJA_DEFINITIVA && !$this->dateRankChecker->isRankValid())
		{
			$this->context->addViolationAtSubPath('estado', $constraint->message, array(), null);
		}
	}
}
