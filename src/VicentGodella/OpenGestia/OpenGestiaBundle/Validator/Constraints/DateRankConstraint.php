<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateRankConstraint extends Constraint
{
    public $message = 'No es posible dar de baja a ninguna persona actualmente';

	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}

	public function validatedBy()
	{
		return 'date_rank_validator';
	}
}
