<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DNIConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
	    if($value == '')
		    return;

        if (preg_match("/^(\d{7,8})\-?([a-zA-Z]{1})$/", $value, $matches)) {
            if($this->letra_nif($matches[1]) != strtoupper($matches[2]))
                $this->context->addViolation($constraint->message, array('%string%' => $value));
        } else {
	        $this->context->addViolation($constraint->message, array('%string%' => $value));
        }
    }

    protected function letra_nif($dni)
    {
        return substr("TRWAGMYFPDXBNJZSQVHLCKE", strtr($dni, "XYZ", "012")%23, 1);
    }
}
