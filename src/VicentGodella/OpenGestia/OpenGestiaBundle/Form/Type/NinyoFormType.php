<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NinyoFormType extends PersonaFormType
{
    public function getName()
    {
        return 'ninyo';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Ninyo'
        );
    }

}
