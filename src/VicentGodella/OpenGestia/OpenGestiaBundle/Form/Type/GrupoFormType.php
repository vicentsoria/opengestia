<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Grupo;

class GrupoFormType extends AbstractType
{
    public function getName()
    {
        return 'grupo';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $builder->add('tipo', 'choice', array(
		    'choices' => Grupo::getTiposDeGrupo(),
	        'attr' => array('placeholder' => 'Etapa')
	    ));
        $builder->add('nombre','text', array(
            'attr' => array('placeholder' => 'Nombre')
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Grupo'
        );
    }

}
