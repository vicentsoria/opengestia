<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona;

class NinyoEditFormType extends PersonaFormType
{
    public function getName()
    {
        return 'ninyo_edit';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);

	    $builder->add('estado', 'choice', array(
			'choices' => Persona::getTiposDeEstado(),
		    'attr' => array('title' => 'Estado')
	    ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Ninyo'
        );
    }

}
