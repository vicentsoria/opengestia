<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BuscarPersonaFormType extends AbstractType
{
    public function getName()
    {
        return 'buscar_persona';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre','text', array(
	        'required' => false
        ));

        $builder->add('apellidos', 'text', array(
	        'required' => false
        ));

	    $builder->add('poblacion', 'text', array(
		    'required' => false
	    ));
    }

//    public function getDefaultOptions(array $options)
//    {
//        return array(
//        );
//    }

}
