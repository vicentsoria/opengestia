<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class EducadorFormType extends PersonaFormType
{
    public function getName()
    {
        return 'educador';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    parent::buildForm($builder, $options);

        $builder->add('estudios', 'text', array(
	        'required' => false,
	        'attr' => array('placeholder' => 'Estudios')
        ));
        $builder->add('profesion', 'text', array(
	        'required' => false,
	        'attr' => array('placeholder' => 'Profesión')
        ));

    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Educador'
        );
    }

}
