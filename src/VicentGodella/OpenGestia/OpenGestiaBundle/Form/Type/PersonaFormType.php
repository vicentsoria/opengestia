<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class PersonaFormType extends AbstractType
{
    public function getName()
    {
        return 'persona';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre','text', array(
	        'attr' => array('placeholder' => 'Nombre')
        ));
        $builder->add('apellidos', null, array(
	        'attr' => array('placeholder' => 'Apellidos')
        ));
        $builder->add('fecha_nacimiento', 'date', array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'invalid_message' => 'Fecha de nacimiento incorrecta',
	        'attr' => array('placeholder' => 'Fecha de nacimiento')
        ));
        $builder->add('direccion', null, array(
	        'attr' => array('placeholder' => 'Dirección')
        ));
        $builder->add('telefono', null, array(
	        'attr' => array('placeholder' => 'Teléfono')
        ));
        $builder->add('codigo_postal', null, array(
	        'attr' => array('placeholder' => 'Código Postal')
        ));
        $builder->add('dni', null, array(
	        'attr' => array('placeholder' => 'DNI')
        ));
        $builder->add('poblacion', null, array(
	        'attr' => array('placeholder' => 'Población')
        ));
        $builder->add('email', 'text', array(
	        'attr' => array('placeholder' => 'Email'),
            'required' => false));

        $builder->add('grupo', 'entity', array(
	        'class' => 'OpenGestiaBundle:Grupo',
	        'property' => 'nombre',
            'query_builder' => function(EntityRepository $er) { //use ($company) {
	            return $er->createQueryBuilder('g');
		    },
	        'attr' => array('title' => 'Grupo')
	    ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona'
        );
    }

}
