# language: es
Característica: RC30-Eliminar-Campos-A-Listado-Niños
	Como responsable de centro
	Quiero eliminar campos al listado de niños
	Para obtener la información con más claridad

	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: eliminamos un campo y ya no aparece en el listado de niños
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
			Y elimino el campo "fechaNacimiento"
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto excepto los siguientes campos:
			"""""""""""""""
			fechaNacimiento
			"""""""""""""""

	@orm
	Escenario: eliminamos un campo desde el desplegable y ya no aparece en el listado de niños
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
			Y elimino el campo "fechaNacimiento" desde el panel
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto excepto los siguientes campos:
			"""""""""""""""
			fechaNacimiento
			"""""""""""""""
