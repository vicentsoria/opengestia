# language: es
Característica: RC2-Listado-Niños-Centro
	Como responsable de centro
	Quiero ver un listado de los niños del centro
	Para saber con cuantos niños cuento actualmente 

 	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: muestra los niños que hay en el centro
		Dado Un usuario autenticado como "responsable de centro"
		 Cuando Estoy en la página de listado de niños de centro
		 Entonces Debería ver los datos de los niños del centro

	@orm
	Escenario: muestra los niños que hay en el centro
		Dado Un usuario autenticado como "responsable de centro"
		  Y No tengo niños en el centro
		 Cuando Estoy en la página de listado de niños de centro
		 Entonces No debería ver niños