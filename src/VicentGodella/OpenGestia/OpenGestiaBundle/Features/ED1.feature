# language: es
Característica: ED1-Guardar-Contacto-Niño
	Como educador
	Quiero guardar datos de contacto de un niño
	Para ponerme en contacto con él

 	Antecedentes:
		Dados prepara antecedentes de educadores

    @orm
	Escenario: guarda datos contacto de niño correctamente
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y Haga click en guardar
		 Entonces Debería ver los datos del niño en el listado de niños del centro

	@orm
	Escenario: muestra error si el teléfono es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y relleno incorrectamente el número de teléfono
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "telefono"
		  Y no haber guardado los datos

	@orm
	Escenario: muestra error si el dni es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y relleno incorrectamente el dni
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "dni"
		  Y no haber guardado los datos

	@orm
	Escenario: muestra error si el correo electrónico es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y relleno incorrectamente el correo electrónico
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "email"
		  Y no haber guardado los datos

	@orm
	Escenario: muestra error si la fecha de nacimiento es incorrecta
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y relleno incorrectamente la fecha de nacimiento
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "fecha_nacimiento"
		  Y no haber guardado los datos

	@orm
	Escenario: Error si coincide el dni del niño con uno existente en el centro
		Dado Un usuario autenticado como "responsable de centro"
		  Y Hay niños en el centro:
	    	| nombre 	| apellidos | fecha_nacimiento 	| direccion 		| cp 	| poblacion | password | email 					 | telefono | dni 		| centro 	    | grupo                 |
    		| enrique	| garcía 	| 2001/05/06 		| C/ Imposible, 3 	| 46001 | Valencia	| secret   | enrique@opengestia.org	 | 96123456 | 12345678Z | mi centro 	| Las marmotas          |
      		| Luis   	| Pérez Gil | 2003/02/19 		| C/ Del medio, 16	| 46100 | Burjassot	| password | luis@opengestia.org	 | 96654231 | 87654321X | mi centro 	| Los lobos solitarios  |
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando rellene los datos del niño correctamente
		  Y introduzca un dni ya existente en el mismo centro
		  Y Haga click en guardar
		 Entonces Debería ver "El dni no se puede repetir en un centro"
		  Y no haber guardado los datos


	@orm
	Escenario: Guardar aceptando el error si coinciden los datos del niño con uno existente en el centro
		Dado Un usuario autenticado como "responsable de centro"
		  Y Hay niños en el centro:
            | nombre 	| apellidos | fecha_nacimiento 	| direccion 		| cp 	| poblacion | password | email 					 | telefono | dni 		| centro 	    | grupo                 |
            | enrique	| garcía 	| 2001/05/06 		| C/ Imposible, 3 	| 46001 | Valencia	| secret   | enrique@opengestia.org	 | 96123456 | 12345678Z | mi centro 	| Las marmotas          |
            | Luis   	| Pérez Gil | 2003/02/19 		| C/ Del medio, 16	| 46100 | Burjassot	| password | luis@opengestia.org	 | 96654231 | 87654321X | mi centro 	| Los lobos solitarios  |
		  Y Estoy en la página de nuevos datos de contacto de niño
		 Cuando introduzca los datos de un niño ya existente en el mismo centro
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de tipo "duplicity_warning"
		  Y Haga click en guardar
		  Y Debería estar en "/ninyo/"