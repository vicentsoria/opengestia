# language: es
Característica: RC7-Baja-Educador
	Como responsable de centro
	Quiero dar de baja un educador
	Para saber de que educadores dispongo actualmente

 	Antecedentes:
		Dados prepara antecedentes

    @orm
	Escenario: marca educador como baja correctamente
		Dados Un usuario autenticado como "responsable de centro"
	  	    Y Estoy en la página de ficha de educador
	   Cuando Marque el educador como baja del curso
		    Y Haga click en guardar
	 Entonces No debería ver los datos del educador en el listado de educadores del centro
