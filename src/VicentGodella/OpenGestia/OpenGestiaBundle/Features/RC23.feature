# language: es
Característica: RC23-Buscar-Persona-Centro
	Como responsable de centro
	Quiero hacer búsquedas sobre personas
	Para acceder a sus datos cómodamente

 	Antecedentes:
		Dados prepara antecedentes

	@orm @javascript
	Escenario: Buscar niño
		 Dado Un usuario autenticado como "responsable de centro"
	   Cuando Estoy en la página de listado de niños de centro
	 Entonces debo ver un elemento ".dataTables_filter input"

	@orm @javascript
	Escenario: Buscar educador
		 Dado Un usuario autenticado como "responsable de centro"
	   Cuando Estoy en la página de listado de educadores de centro
	 Entonces debo ver un elemento ".dataTables_filter input"
