# language: es
Característica: RC26-Marcar-Niño-Baja-Definitiva
    Como responsable de centro
    Quiero marcar un niño como "baja" definitiva
    Para saber cuantos niños tengo a mi cargo actualmente

 	Antecedentes:
		Dados prepara antecedentes

    @orm
	Escenario: marca niño como baja correctamente
		Dado Un usuario autenticado como "responsable de centro"
	  	  Y Estoy en la página de ficha de un niño
	      Y Es un día del rango válido
		 Cuando Lo marque como baja del curso
		  Y Haga click en guardar
		 Entonces No debería ver los datos del niño en el listado de niños del centro

    @orm
	Escenario: no debe marcar el niño como baja si está fuera de rango de fechas
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de ficha de un niño
		  Y Es un día del rango inválido
		 Cuando Lo marque como baja del curso
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "estado"
		  Y no haber guardado los datos
