# language: es
Característica: RC25-Añadir-Campos-A-Listado-Niños
	Como responsable de centro
	Quiero añadir campos al listado de niños
	Para obtener más información de un vistazo

	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: mostrar campos por defecto
		 Dado Un usuario autenticado como "responsable de centro"
		    Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto

	@orm
	Escenario: añadimos un campo y aparece en el listado de niños
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
			Y añado el campo "codigoPostal" para "ninyo"
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto más los siguientes campos:
			"""""""""""""
			codigoPostal
			"""""""""""""

	@orm
	Escenario: añadimos un campo desde el desplegable y aparece en el listado de niños
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
			Y añado el campo "email" para "ninyo"
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto más los siguientes campos:
			"""""""""""""
			email
			"""""""""""""
