# language: es
Característica: RC28-Consultar-Listado-Bajas
	Como responsable de centro
	Quiero ver los usuarios de mi centro que están de "baja definitiva"
	Para dar de alta a cualquier usuario otra vez cuando lo necesite

	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: muestra los niños que hay de baja definitiva en el centro
	      Dado Un usuario autenticado como "responsable de centro"
		     Y Hay 3 niños de baja definitiva
	    Cuando Estoy en la página de listado de niños de centro
		     Y Selecciono niños de baja definitiva desde el panel
	  Entonces Debería ver los datos de los niños de baja definitiva del centro

	@orm
	Escenario: muestra los educadores que hay de baja definitiva en el centro
		Dado Un usuario autenticado como "responsable de centro"
		     Y Hay 2 educadores de baja definitiva
		 Cuando Estoy en la página de listado de niños de centro
			 Y Selecciono educadores de baja definitiva desde el panel
		 Entonces Debería ver los datos de los educadores de baja definitiva del centro