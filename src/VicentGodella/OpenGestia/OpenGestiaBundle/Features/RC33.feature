# language: es
Característica: RC33-Baja-Definitiva-Educador
	Como responsable de centro
	Quiero dar de baja definitiva un educador
	Para saber de que educadores dispongo actualmente

 	Antecedentes:
		Dados prepara antecedentes

    @orm
	Escenario: marca educador como baja correctamente
		Dados Un usuario autenticado como "responsable de centro"
	  	    Y Estoy en la página de ficha de educador
			Y Es un día del rango válido
	   Cuando Marque el educador como baja definitiva del curso
		    Y Haga click en guardar
	 Entonces No debería ver los datos del educador en el listado de educadores del centro

    @orm
	Escenario: no debe marcar el educador como baja si está fuera de rango de fechas
		Dados Un usuario autenticado como "responsable de centro"
			Y Estoy en la página de ficha de educador
			Y Es un día del rango inválido
	   Cuando Marque el educador como baja definitiva del curso
			Y Haga click en guardar
	 Entonces Debería ver un elemento de error para "estado"
			Y no haber guardado los datos del educador
