# language: es
Característica: RC27-Marcar-Usuario-Activo-Desde-Baja-Definitiva
	Como responsable de centro
	Quiero marcar un usuario como activo
	Para saber que usuarios tengo actualmente

	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: marca niño como activo desde baja definitiva
		Dado Un usuario autenticado como "responsable de centro"
		  Y Hay 1 niño de baja definitiva
		  Y Estoy en la página de listado de niños de baja definitiva de centro
		 Cuando Seleccione la ficha del primer niño
		Y Marque el "ninyo" como alta del curso
		  Y Haga click en guardar
		 Entonces Debería ver los datos de los niños del centro

	@orm
	Escenario: marca educador como activo desde baja definitiva
		Dado Un usuario autenticado como "responsable de centro"
		  Y Hay 1 educador de baja definitiva
		  Y Estoy en la página de listado de educadores de baja definitiva de centro
		 Cuando Seleccione la ficha del primer educador
		  Y Marque el "educador" como alta del curso
		  Y Haga click en guardar
		 Entonces Debería ver los datos de los educadores del centro