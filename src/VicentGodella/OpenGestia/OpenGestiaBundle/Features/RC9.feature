# language: es
Característica: RC9-Ascender-Niño-Educador
	Como responsable de centro
	Quiero ascender a un niño a educador
	Para saber de que educadores dispongo actualmente

	Antecedentes:
		Dados prepara antecedentes

	@orm @javascript
	Escenario:
		Dado Un usuario autenticado como "responsable de centro"
		   Y Estoy en la página de ficha de un niño mediante ajax
	  Cuando Hago click en ascender niño
		   Y Confirmo la acción de ascender
	Entonces No debería ver los datos del niño en el listado de niños del centro

	@orm
	Escenario: Error 404 si no existe el niño
		Dado Un usuario autenticado como "responsable de centro"
	  Cuando Intento ascender un niño que no existe
	Entonces el código de estado de la respuesta debe ser 404