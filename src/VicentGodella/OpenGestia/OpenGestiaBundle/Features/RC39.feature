# language: es
Característica: RC39-Baja-Definitiva-Niños
	Como responsable de centro
  Quiero dar de baja definitiva varios niños simultáneamente
    Para ejecutar la baja definitiva de varios niños cómodamente

 	Antecedentes:
		Dados prepara antecedentes

    @orm @javascript
	Escenario: marca niños como baja definitivas correctamente
		Dados Un usuario autenticado como "responsable de centro"
	  	    Y Estoy en la página de listado de niños de centro
	   Cuando selecciono una persona del listado
		    Y hago click en baja definitiva desde el panel
	 Entonces No debería ver los datos del niño en el listado de niños del centro
