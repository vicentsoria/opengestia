# language: es
Característica: RC4-Guardar-Contacto-Educador
	Como responsable de centro
	Quiero guardar los datos de contacto de un educador
	Para ponerme en contacto con él

 	Antecedentes:
		Dados prepara antecedentes

    @orm
	Escenario: guarda datos contacto de educador correctamente
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de educador
		 Cuando rellene los datos del educador correctamente
		  Y Haga click en guardar
		 Entonces Debería ver los datos del educador en el listado de educadores del centro
		 
    @orm
	Escenario: muestra error si el teléfono del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de educador
		 Cuando relleno incorrectamente el número de teléfono del educador
		  Y Haga click en guardar
		 Entonces Debería ver "Número de teléfono incorrecto"
		  Y no haber guardado los datos del educador

	@orm
	Escenario: muestra error si el dni del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de educador
		 Cuando rellene los datos del educador correctamente
		  Y relleno incorrectamente el dni del educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "dni"
		  Y no haber guardado los datos del educador

	@orm
	Escenario: muestra error si el correo electrónico del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de educador
		 Cuando rellene los datos del educador correctamente
		  Y relleno incorrectamente el correo electrónico del educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "email"
		  Y no haber guardado los datos

	@orm
	Escenario: muestra error si la fecha de nacimiento del educador es incorrecta
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de nuevos datos de contacto de educador
		 Cuando rellene los datos del educador correctamente
		  Y relleno incorrectamente la fecha de nacimiento del educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "fecha_nacimiento"
		  Y no haber guardado los datos