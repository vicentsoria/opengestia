<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Educador;
use VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type\EducadorFormType;
use VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type\EducadorEditFormType;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\ConfiguracionUsuario;

class EducadorController extends Controller
{
    public function indexAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $educadores = $em->getRepository('OpenGestiaBundle:Educador')->findActivos();

	    $campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

        return $this->render('OpenGestiaBundle:Educador:index.html.twig', array(
                'educadores' => $educadores,
		        'campos_disponibles' => $campos_disponibles['educadores']
            )
        );
    }

	public function bajasDefinitivasAction()
	{
		$em = $this->get('doctrine')->getEntityManager();
		$educadores = $em->getRepository('OpenGestiaBundle:Educador')->findBajasDefinitivas();

		$campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

		return $this->render('OpenGestiaBundle:Educador:index.html.twig', array(
				'educadores' => $educadores,
				'campos_disponibles' => $campos_disponibles['educadores']
			)
		);
	}

	public function bajasAction()
	{
		$em = $this->get('doctrine')->getEntityManager();
		$educadores = $em->getRepository('OpenGestiaBundle:Educador')->findBajas();

		$campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

		return $this->render('OpenGestiaBundle:Educador:index.html.twig', array(
				'educadores' => $educadores,
				'campos_disponibles' => $campos_disponibles['educadores']
			)
		);
	}

    public function bajasSimultaneasAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $personaManager = $this->get('opengestia.persona_manager');

        $educadoresIds = $this->getRequest()->request->get('ids');

        $educadores = $em->getRepository('OpenGestiaBundle:Educador')->findEducadoresByIds($educadoresIds);

        foreach ($educadores as $educador) {
            $personaManager->darDeBaja($educador);
        }

        return new Response('');
    }

    public function bajasDefinitivasSimultaneasAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $personaManager = $this->get('opengestia.persona_manager');

        $educadoresIds = $this->getRequest()->request->get('ids');

        $educadores = $em->getRepository('OpenGestiaBundle:Educador')->findEducadoresByIds($educadoresIds);

        foreach ($educadores as $educador) {
            $personaManager->darDeBajaDefinitiva($educador);
        }

        return new Response('');
    }

    public function newAction()
    {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $duplicidad = false;

        $user = $this->get('security.context')->getToken()->getUser();

        $persona = new Educador();
        $persona->setCentro($user->getCentro());
        $persona->setType('educador');

        $formulario = $this->createForm(new EducadorFormType(), $persona);

        if ($request->getMethod() == 'POST') {
            $formulario->bind($request);

            if ($formulario->isValid()) {
                if(!$request->get('duplicidad') && $this->hayDuplicidadEnCentro($em, $persona)) {
                    $this->get('session')->setFlash('warning', 'Es posible que esta persona ya esté en el centro, debes confirmar el guardado');

                    $duplicidad = true;
                } else {
                    $this->get('session')->setFlash('notice', 'Persona creada satisfactoriamente');

                    $em->persist($persona);
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('educador_index'));   
                }
            }
        }


	    $template = 'OpenGestiaBundle:Educador:new.html.twig';

	    if ($request->isXmlHttpRequest()) {
		    $template = 'OpenGestiaBundle:Educador:new_form.html.twig';
	    }


        return $this->render($template,
            array(
                'formulario' => $formulario->createView(),
                'duplicidad' => $duplicidad
            )
        );
    }

	public function editAction($id)
	{
		$request = $this->get('request');
		$em = $this->get('doctrine')->getEntityManager();

		$persona = $this->getDoctrine()->getManager()->getRepository('OpenGestiaBundle:Educador')->find($id);

		$formulario = $this->createForm(new EducadorEditFormType(), $persona);

		if ($request->getMethod() == 'POST') {
			$formulario->bind($request);

			if ($formulario->isValid()) {
				$this->get('session')->setFlash('notice', 'Se han guardado los datos del educador satisfactoriamente');

				$em->persist($persona);
				$em->flush();

				return $this->redirect($this->generateUrl('educador_index'));
			}
		}

		$template = 'OpenGestiaBundle:Educador:edit.html.twig';

		if ($request->isXmlHttpRequest()) {
			$template = 'OpenGestiaBundle:Educador:edit_form.html.twig';
		}

		return $this->render($template,
			array(
				'formulario' => $formulario->createView()
			)
		);
	}

	public function anyadirCampoAction($campo)
	{
		$em = $this->get('doctrine')->getEntityManager();

		$usuario = $this->container->get('security.context')->getToken()->getUser();

		$configuracionUsuario = $usuario->getConfiguracion();
		$configuracionUsuario->anyadirCampo('educadores', $campo);

		$em->persist($configuracionUsuario);
		$em->flush($configuracionUsuario);

		return $this->indexAction();
	}

	public function eliminarCampoAction($campo)
	{
		$em = $this->get('doctrine')->getEntityManager();

		$usuario = $this->container->get('security.context')->getToken()->getUser();

		$configuracionUsuario = $usuario->getConfiguracion();
		$configuracionUsuario->eliminarCampo('educadores', $campo);

		$em->persist($configuracionUsuario);
		$em->flush($configuracionUsuario);

		return $this->indexAction();
	}

	public function hayDuplicidadEnCentro($em, $persona)
    {
    }
}
