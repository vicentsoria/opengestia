<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Grupo;
use VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type\GrupoFormType;

class GrupoController extends Controller
{
	public function indexAction()
	{
		$em = $this->get('doctrine')->getEntityManager();
		$user = $this->get('security.context')->getToken()->getUser();

		$grupos = $em->getRepository('OpenGestiaBundle:Grupo')->findByCentro($user->getCentro());

		return $this->render('OpenGestiaBundle:Grupo:index.html.twig', array(
				'grupos' => $grupos
			)
		);
	}


	public function newAction()
    {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $grupo = new Grupo();
	    $grupo->setCentro($user->getCentro());

        $formulario = $this->createForm(new GrupoFormType(), $grupo);

        if ($request->getMethod() == 'POST') {
            $formulario->bind($request);

            if ($formulario->isValid()) {
                $this->get('session')->setFlash('notice', 'Se ha creado el grupo satisfactoriamente');

                $em->persist($grupo);
                $em->flush();

                return $this->redirect($this->generateUrl('grupo_index'));
            }
        }

        $template = 'OpenGestiaBundle:Grupo:new.html.twig';

        if ($request->isXmlHttpRequest()) {
            $template = 'OpenGestiaBundle:Grupo:new_form.html.twig';
        }

        return $this->render($template,
            array(
                'formulario' => $formulario->createView()
            )
        );
    }

}
