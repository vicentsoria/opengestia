<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;


class DefaultController extends Controller
{
    public function indexAction()
    {
	    return new RedirectResponse('ninyo');
    }
}
