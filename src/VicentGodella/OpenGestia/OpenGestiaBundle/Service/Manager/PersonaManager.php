<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Service\Manager;

use Doctrine\ORM\EntityManager;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class PersonaManager
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function darDeBaja(Persona $persona)
    {
        $persona->setEstado(Persona::ESTADO_BAJA);
        $this->em->persist($persona);
        $this->em->flush();
    }

    public function darDeBajaDefinitiva(Persona $persona)
    {
        $persona->setEstado(Persona::ESTADO_BAJA_DEFINITIVA);
        $this->em->persist($persona);
        $this->em->flush();
    }

    public function existePersonaEnCentro(Persona $persona)
    {
        $repository = $this->em->getRepository('OpenGestiaBundle:Persona');

        $personas = $repository->findBy(array(
            'nombre' => $persona->getNombre(),
            'apellidos' => $persona->getApellidos(),
            'fecha_nacimiento' => $persona->getFechaNacimiento(),
        ));

        if(count($personas) > 0)
            return true;

        return false;
    }
}
