<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Service;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class Dater
{
	function __construct()
	{

	}

	public function getCurrentTime($format = 'd/m')
	{
		$now =  new \DateTime();

		return $now->format($format);
	}
}
