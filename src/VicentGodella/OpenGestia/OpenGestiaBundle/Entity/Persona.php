<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints as OpenGestiaAssert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * VicentGodella\OpenGestiaBundle\Entity\Persona
 *
 * @ORM\Table(name="Persona")
 * @ORM\Entity(repositoryClass="VicentGodella\OpenGestia\OpenGestiaBundle\Entity\PersonaRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({ "educador" = "Educador", "ninyo" = "Ninyo" })
 *
 * @UniqueEntity(fields="email", message="El email ya existe")
 * @UniqueEntity(fields={"dni","centro"}, message="El dni no se puede repetir en un centro")
 * @OpenGestiaAssert\DateRankConstraint
 */
class Persona implements UserInterface, \Serializable
{
	const ESTADO_ACTIVO = 0;
	const ESTADO_BAJA = 1;
	const ESTADO_BAJA_DEFINITIVA = 2;

	public static function getTiposDeEstado()
	{
		return array(
			self::ESTADO_ACTIVO => 'Activo',
			self::ESTADO_BAJA => 'Baja',
			self::ESTADO_BAJA_DEFINITIVA => 'Baja definitiva'
		);
	}

    /**
     * Método requerido por la interfaz UserInterface
     */
    public function equals(\Symfony\Component\Security\Core\User\UserInterface $usuario)
    {
        return $this->getEmail() == $usuario->getEmail();
    }

    /**
     * Método requerido por la interfaz UserInterface
     */
    public function eraseCredentials()
    {
    }

    /**
     * Método requerido por la interfaz UserInterface
     */
    public function getRoles()
    {
        return array('ROLE_USUARIO');
    }

    /**
     * Método requerido por la interfaz UserInterface
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    public function serialize()
    {
        return serialize(array(
            $this->id
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id
        ) = unserialize($serialized);
    }

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="nombre", type="string")
     * @Assert\NotBlank()
     */
    protected $nombre;

    /**
     * @ORM\Column(name="apellidos", type="string")
     * @Assert\NotBlank()
     */
    protected $apellidos;

    /**
     * @ORM\Column(name="fecha_nacimiento", type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $fecha_nacimiento;

    /**
     * @ORM\Column(name="direccion", type="string")
     * @Assert\NotBlank()
     */
    protected $direccion;

    /**
     * @ORM\Column(name="codigo_postal", type="string")
     * @Assert\NotBlank()
     */
    protected $codigo_postal;

    /**
     * @ORM\Column(name="email", type="string", nullable=true, unique=true)
     * @Assert\Email(message="Correo electrónico incorrecto")
     */
    protected $email;

    /**
     * @ORM\Column(name="password", type="string")
     */
    protected $password = '';


    /**
     * @ORM\Column(name="poblacion", type="string")
     * @Assert\NotBlank()
     */
    protected $poblacion;

    /**
     * @ORM\Column(name="dni", type="string", nullable=true)
     * @OpenGestiaAssert\DNIConstraint
     */
    protected $dni;

    /**
     * @ORM\Column(name="telefono", type="string", nullable=true)
     * @Assert\Regex(
     *     pattern="/^\d{8,9}$/",
     *     match=true,
     *     message="Número de teléfono incorrecto"
     * )
     */
    protected $telefono;

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="educadores")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     */
    protected $grupo;

    /**
     * @ORM\Column(name="centro", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $centro = '';

	/**
	 * @ORM\Column(name="estado", type="integer")
	 */
	protected $estado = 0;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt = '';

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param  integer $nombre
     * @return Persona
     */
    public function setNombre($nombre)
    {
        $this->nombre = ucfirst($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return integer
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param  string  $apellidos
     * @return Persona
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = ucfirst($apellidos);

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set fecha_nacimiento
     *
     * @param  datetime $fechaNacimiento
     * @return Persona
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fecha_nacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fecha_nacimiento
     *
     * @return datetime
     */
    public function getFechaNacimiento()
    {
        return $this->fecha_nacimiento;
    }

    /**
     * Set direccion
     *
     * @param  string  $direccion
     * @return Persona
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set codigo_postal
     *
     * @param  string  $codigoPostal
     * @return Persona
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigo_postal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigo_postal
     *
     * @return string
     */
    public function getCodigoPostal()
    {
        return $this->codigo_postal;
    }

    /**
     * Set poblacion
     *
     * @param  string  $poblacion
     * @return Persona
     */
    public function setPoblacion($poblacion)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion
     *
     * @return string
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set email
     *
     * @param  string  $email
     * @return Persona
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param  string  $password
     * @return Persona
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set type
     *
     * @param  string  $type
     * @return Persona
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set salt
     *
     * @param  string  $salt
     * @return Persona
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set telefono
     *
     * @param  string  $telefono
     * @return Persona
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set dni
     *
     * @param  string  $dni
     * @return Persona
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set grupo
     *
     * @param  string  $grupo
     * @return Persona
     */
    public function setGrupo(Grupo $grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return string
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set centro
     *
     * @param  string  $centro
     * @return Persona
     */
    public function setCentro($centro)
    {
        $this->centro = $centro;

        return $this;
    }

    /**
     * Get centro
     *
     * @return string
     */
    public function getCentro()
    {
        return $this->centro;
    }

	public function setEstado($estado)
	{
		$this->estado = $estado;
	}

	public function getEstado()
	{
		return $this->estado;
	}
}