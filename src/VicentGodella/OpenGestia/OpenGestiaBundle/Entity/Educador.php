<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints as OpenGestiaAssert;

/**
 * VicentGodella\OpenGestiaBundle\Entity\Persona
 *
 * @ORM\Entity(repositoryClass="VicentGodella\OpenGestia\OpenGestiaBundle\Entity\EducadorRepository")
 */
class Educador extends Persona implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="estudios", type="string")
     */
    protected $estudios;

    /**
     * @ORM\Column(name="profesion", type="string")
     */
    protected $profesion;

	/**
	 * @ORM\OneToOne(targetEntity="ConfiguracionUsuario", cascade={"all"})
	 * @ORM\JoinColumn(name="configuracion_usuario_id", referencedColumnName="id")
	 */
	protected $configuracion;

	public function __construct()
	{
		parent::__construct();

		$this->setConfiguracion(new ConfiguracionUsuario());
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getTipoPersona()
	{
		return 'educador';
	}
    /**
     * Set estudios
     *
     * @param string $estudios
     * @return Educador
     */
    public function setEstudios($estudios)
    {
        $this->estudios = $estudios;
        return $this;
    }

    /**
     * Get estudios
     *
     * @return string 
     */
    public function getEstudios()
    {
        return $this->estudios;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Educador
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;
        return $this;
    }

    /**
     * Get profesion
     *
     * @return string 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

	/**
	 * Set configuracion
	 *
	 * @param ConfiguracionUsuario $configuracion
	 */
	public function setConfiguracion(ConfiguracionUsuario $configuracion)
	{
		$this->configuracion = $configuracion;
	}

	/**
	 * Get configuracion
	 *
	 * @return ConfiguracionUsuario
	 */
	public function getConfiguracion()
	{
		return $this->configuracion;
	}
}