<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints as OpenGestiaAssert;

/**
 * VicentGodella\OpenGestiaBundle\Entity\ConfiguracionUsuario
 *
 * @ORM\Table(name="ConfiguracionUsuario")
 * @ORM\Entity()
 */
class ConfiguracionUsuario
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @ORM\Column(name="configuracion", type="array")
	 */
	protected $configuracion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	public function __construct()
	{
		$this->configuracion = $this->getDefaultConfiguracion();
	}

	public function setConfiguracion($configuracion)
	{
		$this->configuracion = $configuracion;
	}

	public function getCampos($listado = null)
	{
		if($listado === null)
			return $this->configuracion;
		else
			return $this->configuracion[$listado];

	}

	public function anyadirCampo($listado, $campo)
	{
		if(!in_array($campo, $this->configuracion[$listado]))
			$this->configuracion[$listado][] = $campo;
	}

	public function eliminarCampo($listado, $campo)
	{
		foreach($this->configuracion[$listado] as $key => $value)
		{
			if($value === $campo)
				unset($this->configuracion[$listado][$key]);
		}
	}

	public function getDefaultConfiguracion()
	{
		return array(
			'ninyos' => array(
				'nombre', 'apellidos', 'fechaNacimiento', 'direccion', 'telefono', 'grupo'
			),
			'educadores' => array(
				'nombre', 'apellidos', 'fechaNacimiento', 'direccion', 'telefono', 'grupo'
			)
		);
	}

	static public function getCamposDisponibles()
	{
		return array(
			'ninyos' => array(
				'nombre', 'apellidos', 'fechaNacimiento', 'direccion', 'telefono', 'grupo', 'codigoPostal', 'dni', 'email', 'cuotaPagada'
			),
			'educadores' => array(
				'nombre', 'apellidos', 'fechaNacimiento', 'direccion', 'telefono', 'grupo', 'codigoPostal', 'dni', 'email'
			)
		);
	}
}