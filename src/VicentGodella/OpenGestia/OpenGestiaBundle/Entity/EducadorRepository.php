<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\EntityRepository;

class EducadorRepository extends EntityRepository
{
	public function findActivos()
	{
		return $this->findByEstado(Persona::ESTADO_ACTIVO);
	}

	public function findBajasDefinitivas()
	{
		return $this->findByEstado(Persona::ESTADO_BAJA_DEFINITIVA);
	}

	public function findBajas()
	{
		return $this->findByEstado(Persona::ESTADO_BAJA);
	}

	public function findByEstado($estado)
	{
		$query = $this->getEntityManager()->getRepository('OpenGestiaBundle:Educador')->createQueryBuilder('n')
			->where('n.estado= :estado')
			->setParameter('estado', $estado)
			->getQuery();

		return $query->getResult();
	}

    public function findEducadoresByIds($ids)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT e FROM OpenGestiaBundle:Educador e WHERE e.id IN('.$ids.')');

        return $query->getResult();
    }
}
